peek (1.5.1+git20230114-1) unstable; urgency=medium

  * QA upload.
  * Orphan the package. (See #1030266)
  * New upstream git snapshot declaring project deprecation.
  * debian/control: Bump Standards-Version to 4.6.2.

 -- Boyuan Yang <byang@debian.org>  Wed, 01 Feb 2023 14:34:03 -0500

peek (1.5.1+git20211214-1) unstable; urgency=medium

  * New upstream git snapshot.
  * debian/patches: Drop all patches, merged upstream.
  * debian/control: Bump Standards-Version to 4.6.0.
  * debian/copyright: Update copyright information.

 -- Boyuan Yang <byang@debian.org>  Fri, 31 Dec 2021 13:28:38 -0500

peek (1.5.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on gettext,
      libglib2.0-dev and libgtk-3-dev.

  [ Boyuan Yang ]
  * debian/patches/0002-Add-support-for-gnome40.patch: Support GNOME 40+.
  * debian/patches/0001: Add "Forwarded" tag.

 -- Boyuan Yang <byang@debian.org>  Mon, 06 Dec 2021 14:06:44 -0500

peek (1.5.1-2) unstable; urgency=medium

  * Rebuild after Debian 11 release.
  * Bump debhelper compat to v13.
  * debian/patches/0001: Use python3 command instead of python in
    po/meson.build. Python command is now unavailable.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Boyuan Yang <byang@debian.org>  Sun, 05 Sep 2021 12:49:25 -0400

peek (1.5.1-1) unstable; urgency=high

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Thu, 20 Feb 2020 11:59:44 -0500

peek (1.5.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.
  * Set upstream metadata fields: Repository.

  [ Boyuan Yang ]
  * New upstream release 1.5.0.
  * debian/patches: Drop all backported patches.
  * debian/upstream/metadata: Add Repository-Browse.
  * debian/copyright: Refresh information.

 -- Boyuan Yang <byang@debian.org>  Tue, 18 Feb 2020 11:59:45 -0500

peek (1.4.0-2) unstable; urgency=medium

  * Backport upstream trunk commits till 2019-09-25.
  * debian/patches/0001: Dropped, merged upstream.

 -- Boyuan Yang <byang@debian.org>  Fri, 27 Sep 2019 12:10:10 -0400

peek (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0.
  * debian/patches: Drop patches merged upstream (0001, 0003).
  * debian/rules: Switch to meson buildsystem.
  * debian/control:
    + Bump Standards-Version to 4.4.0.
    + Add optional build-dependency desktop-file-utils. The
      appstream-utils is not included since the related checks
      requires network connection and write permission to home
      directory.

 -- Boyuan Yang <byang@debian.org>  Tue, 24 Sep 2019 10:57:12 -0400

peek (1.3.1-6) unstable; urgency=high

  * debian/patches: Add patch 0003 to fix double free crashing when
    passing string array to async function. (Closes: #926386)

 -- Boyuan Yang <byang@debian.org>  Mon, 06 May 2019 13:27:48 -0400

peek (1.3.1-5) unstable; urgency=medium

  * Rebuild for Debian buster.
  * debian/patches:
    + Backport upstream trunk code till 20190121.
    + Bump Standards-Version to 4.3.0.
    + Bump debhelper compat to v12.
    + R³: Use "Rules-Requires-Root: no".

 -- Boyuan Yang <byang@debian.org>  Fri, 08 Feb 2019 13:14:08 -0500

peek (1.3.1-4) unstable; urgency=medium

  * Rebuild against gcc 8.
  * debian/control:
    + Update maintainer email address and use the @debian.org one.
    + Bump Standards-Version to 4.2.1 (no changes needed).

 -- Boyuan Yang <byang@debian.org>  Thu, 13 Sep 2018 15:57:12 -0400

peek (1.3.1-3) unstable; urgency=medium

  * Backport upstream changes till 20180613.
    - Fixes FTBFS with parallel build. Closes: #895789.
  * d/rules: Enable parallel build again.
  * d/patches: Add a patch to fix appstream metainfo warning.

 -- Boyuan Yang <073plan@gmail.com>  Fri, 15 Jun 2018 20:47:23 +0800

peek (1.3.1-2) unstable; urgency=medium

  * d/control: Update Vcs fields.
  * d/control: Bump Standards-Version to 4.1.4 (no changes needed).
  * d/control: Mark peek suggests gifski.
  * Backport upstream changes till 20180416.

 -- Boyuan Yang <073plan@gmail.com>  Sat, 21 Apr 2018 14:06:48 +0800

peek (1.3.1-1) unstable; urgency=medium

  * Initial release. Closes: #855666

 -- Boyuan Yang <073plan@gmail.com>  Wed, 28 Mar 2018 09:27:34 +0800
